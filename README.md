# Wordle solver (CPSC 210 project)

This project aims to help win Wordle. The user enters their current guess and the hints provided by Wordle, and the
program looks through a list of words and finds every possible word that the solution could be. This is intended to be
used either by people who get stuck, or by those who enjoy the satisfaction of performing well in single player games
with minimal effort. This project is interesting to me because:

* I sometimes find it satisfying to have low effort ways to beat games
* Handling the case of a letter being in the word but not being in the right place seems like a useful problem to solve in general.

## Acknowledgements
This program uses a word list modified from the [sindresorhus/word-list](https://github.com/sindresorhus/word-list)
GitHub repository. That repository is licensed under the MIT License. The word list in this program is derived from
[words.txt](https://raw.githubusercontent.com/sindresorhus/word-list/main/words.txt), with all words that are longer
or shorter than 5 letters filtered out.

## User stories
### Phase 1

- [X] As a user, I want the program to automatically choose a first guess to add to the list of guesses that provides a lot of information.
- [X] As a user, I want to be able to choose my own first guess to add to the list of guesses.
- [X] As a user, I want to be able to enter hints given by the game, and get suggestions for next guesses to add to the list of guesses made so far.
- [X] As a user, I want to be able to add multiple guesses to the list at once at the start in case I started a game before using the solver.

### Phase 2

- [X] As a user, I want to be able to save lists of guesses to come back to later.
- [X] As a user, I want to be able to load a saved list of guesses. 
- [X] As a user, I want to view statistics on how the algorithm performs.
- [X] As a user, I want to be able to flag suggestions that Wordle considers invalid to prevent them from being suggested in the future.

### Phase 3

Same stuff as before, except this applies to the GUI rather than the console UI.

- [X] As a user, I want the program to automatically choose a first guess to add to the list of guesses that provides a lot of information.
- [X] As a user, I want to be able to choose my own first guess to add to the list of guesses.
- [X] As a user, I want to be able to enter hints given by the game, and get suggestions for next guesses to add to the list of guesses made so far.
- [X] As a user, I want to be able to add multiple guesses to the list at once at the start in case I started a game before using the solver.
- [X] As a user, I want to be able to save lists of guesses to come back to later.
- [X] As a user, I want to be able to load a saved list of guesses.
- [X] As a user, I want to view statistics on how the algorithm performs.
- [X] As a user, I want to be able to flag suggestions that Wordle considers invalid to prevent them from being suggested in the future.

### Phase 4

Implement logging for the following user stories from phase 3

- [X] As a user, I want to add a guess to the list of guesses. Generalization of the following user stories:
  - [X] As a user, I want the program to automatically choose a first guess to add to the list of guesses that provides a lot of information.
  - [X] As a user, I want to be able to choose my own first guess to add to the list of guesses.
  - [X] As a user, I want to be able to add multiple guesses to the list at once at the start in case I started a game before using the solver.
- [X] As a user, I want to be able to enter hints given by the game, and get suggestions for next guesses to add to the list of guesses made so far. (This modifies the guess and other guesses in the list that depend on it)

Typical log:

```
Tue Mar 29 00:52:21 PDT 2022
Guessing kanes
Tue Mar 29 00:52:25 PDT 2022
Letter 3 of guess 0 said to be in the wrong place
Tue Mar 29 00:52:26 PDT 2022
Letter 3 of guess 0 said to be correct
Tue Mar 29 00:53:24 PDT 2022
Guessing curet
Tue Mar 29 00:53:26 PDT 2022
Letter 3 of guess 1 said to be in the wrong place
Tue Mar 29 00:53:26 PDT 2022
Letter 3 of guess 1 said to be correct
Tue Mar 29 00:53:28 PDT 2022
Letter 4 of guess 1 said to be in the wrong place
Tue Mar 29 00:53:47 PDT 2022
Guessing toled
Tue Mar 29 00:53:48 PDT 2022
Letter 0 of guess 2 said to be in the wrong place
Tue Mar 29 00:53:49 PDT 2022
Letter 0 of guess 2 said to be correct
Tue Mar 29 00:53:50 PDT 2022
Letter 1 of guess 2 said to be in the wrong place
Tue Mar 29 00:53:50 PDT 2022
Letter 1 of guess 2 said to be correct
Tue Mar 29 00:53:52 PDT 2022
Letter 3 of guess 2 said to be in the wrong place
Tue Mar 29 00:53:52 PDT 2022
Letter 3 of guess 2 said to be correct
Tue Mar 29 00:54:11 PDT 2022
Guessing totem
Tue Mar 29 00:54:12 PDT 2022
Letter 0 of guess 3 said to be in the wrong place
Tue Mar 29 00:54:13 PDT 2022
Letter 0 of guess 3 said to be correct
Tue Mar 29 00:54:13 PDT 2022
Letter 1 of guess 3 said to be in the wrong place
Tue Mar 29 00:54:13 PDT 2022
Letter 1 of guess 3 said to be correct
Tue Mar 29 00:54:14 PDT 2022
Letter 2 of guess 3 said to be in the wrong place
Tue Mar 29 00:54:14 PDT 2022
Letter 2 of guess 3 said to be correct
Tue Mar 29 00:54:15 PDT 2022
Letter 3 of guess 3 said to be in the wrong place
Tue Mar 29 00:54:15 PDT 2022
Letter 3 of guess 3 said to be correct
Tue Mar 29 00:54:15 PDT 2022
Letter 4 of guess 3 said to be in the wrong place
Tue Mar 29 00:54:15 PDT 2022
Letter 4 of guess 3 said to be correct
```

## Phase 4: Task 3

[!UML_Design_Diagram.png]

* The UI abstract class was originally intended to contain code common between the console and GUI interfaces, with those classes just implementing what was different between them.
  However, it turned out that all they had in common were a few constants and fields. That means that there is minimal benefit to having it there. 
  If I had more time, I would either get rid of it or refactor the code so there is more common code that can be reused with an abstract class.
* Because the classes in the persistence method are not instantiated by anything and do not maintain references to anything (because they exist solely as containers for static helper methods), it is not clear on the diagram how they are related to anything.
  If I had more time, I would think of a way to change the relationship so it is more visible on the class diagram.
* ControlButtonHandler, HintAreaUpdater, and WordleWindowListener are dependent on some static fields and methods in Graphical. It might be better to have bidirectional relationships to make this more clear.
  (This would also require instantiating Graphical.)
* It is not clear what uses EventLog because no references to it are maintained. It might be better to use a field to contain the result of EventLog.getInstance() rather than calling getInstance() every time.