package persistence;

import org.json.JSONArray;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import persistence.WordList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class WordListTest {

    @Test
    void testLoadWordsSavedList() {
        try {
            List<String> expected = new ArrayList<String>();
            expected.add("aaaaa");
            expected.add("bbbbb");
            SimpleFile.writeFile(WordList.SAVED_WORD_LIST_PATH, new JSONArray(expected).toString());

            List<String> actual = WordList.loadWords();
            assertEquals(expected, actual);
        } catch (IOException e) {
            fail("IOException not expected!");
            e.printStackTrace();
        }
    }

    @Test
    //REQUIRES: AfterEach has run and deleted the saved list, so this test must not be first
    void testLoadWordsNoSavedList() {
        try {
            List<String> expected = new ArrayList<String>();
            File file = new File(WordList.INITIAL_WORD_LIST_PATH);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                expected.add(scanner.nextLine());
            }
            List<String> actual = WordList.loadWords();
            assertEquals(expected, actual);

            List<String> savedWorklist = WordList.loadSaved();
            assertEquals(expected, savedWorklist);
        } catch (FileNotFoundException e) {
            fail("Failed to read word list!");
            e.printStackTrace();
        } catch (IOException e) {
            fail("Failed to write word list");
            e.printStackTrace();
        }
    }

    @Test
    void testLoadSaved() {
        try {
            List<String> expected = new ArrayList<String>();
            expected.add("aaaaa");
            expected.add("bbbbb");
            SimpleFile.writeFile(WordList.SAVED_WORD_LIST_PATH, new JSONArray(expected).toString());

            List<String> actual = WordList.loadSaved();
            assertEquals(expected, actual);
        } catch (IOException e) {
            fail("IOException not expected!");
            e.printStackTrace();
        }
    }

    @Test
    void testLoadInitial() {
        try {
            List<String> expected = new ArrayList<String>();
            File file = new File(WordList.INITIAL_WORD_LIST_PATH);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                expected.add(scanner.nextLine());
            }
            List<String> actual = WordList.loadInitial();
            assertEquals(expected, actual);

            List<String> savedWorklist = WordList.loadSaved();
            assertEquals(expected, savedWorklist);
        } catch (FileNotFoundException e) {
            System.out.println("Failed to read word list!");
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            System.out.println("Failed to write word list! WIll not be able to modify it later.");
        }
    }

    @Test
    void testDeleteWord() {
        List<String> initial = new ArrayList<String>();
        initial.add("aaaaa");
        final String DELETE = "bbbbb";
        initial.add(DELETE);
        try {
            SimpleFile.writeFile(WordList.SAVED_WORD_LIST_PATH, new JSONArray(initial).toString());
        } catch (IOException e) {
            fail("Unexpected IOException thrown");
            e.printStackTrace();
        }

        List<String> expected = new ArrayList<String>();
        expected.addAll(initial);
        expected.remove(DELETE);

        List<String> actual = null;
        try {
            actual = WordList.loadSaved();
        } catch (FileNotFoundException e) {
            fail("Unexpected FileNotFoundException thrown");
            e.printStackTrace();
        }
        try {
            WordList.deleteWord(DELETE);
        } catch (IOException e) {
            fail("Unexpected IOException thrown");
            e.printStackTrace();
        }
        assertEquals(expected, actual);
        try {
            assertEquals(expected, WordList.loadSaved());
        } catch (FileNotFoundException e) {
            fail("Unexpected FileNotFoundException thrown");
            e.printStackTrace();
        }
    }

    @AfterEach
    void deleteSavedWordList() {
        File file = new File(WordList.SAVED_WORD_LIST_PATH);
        file.delete();
    }
}