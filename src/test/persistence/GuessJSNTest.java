package persistence;

import model.Guess;
import model.GuessList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class GuessJSNTest {

    @BeforeEach
    void setup() {
        try {
            WordList.loadWords();
        } catch (IOException e) {
            fail("Unexpected IOException!");
            e.printStackTrace();
        }
    }

    @Test
    void testEncodeAndDecode() {
        List<Integer> hints = new ArrayList<Integer>();
        for (int i = 0; i < Guess.WORD_LENGTH; i++) {
            hints.add(Guess.HINT_NOT_IN_WORD);
        }
        Guess expected = new Guess("abcde", hints);
        Guess actual = GuessJSN.fromJSNtoGuess(
                GuessJSN.guessToJSN(expected)
        );

        assertEquals(expected, actual);
    }
}
