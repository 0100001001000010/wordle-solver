package persistence;

import model.Guess;
import model.GuessList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GuessListJSNTest {

    @Test
    void testSaveAndLoad() {
        GuessList expected = new GuessList(6);
        List<Integer> hints = new ArrayList<Integer>();
        for (int i = 0; i < Guess.WORD_LENGTH; i++) {
            hints.add(Guess.HINT_NOT_IN_WORD);
        }
        expected.add(new Guess("abcde", hints));
        try {
            GuessListJSN.saveGuessList(expected);
        } catch (IOException e) {
            fail("Unexpected IOException thrown");
            e.printStackTrace();
        }
        GuessList actual = null;
        try {
            actual = GuessListJSN.loadGuessList();
        } catch (FileNotFoundException e) {
            fail("Unexpected FileNotFound exception thrown");
            e.printStackTrace();
        }
        assertEquals(expected, actual);
    }

    @AfterEach
    void cleanup() {
        File file = new File(GuessListJSN.PATH);
        file.delete();
    }
}