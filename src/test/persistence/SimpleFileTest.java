package persistence;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class SimpleFileTest {
    private final String FILE = "data/test.txt";
    private final String CONTENTS = "Hello!\n"; // Needs to have a \n at the end because that gets added to files if not
                                                // present

    @Test
    void testWriteThenRead(){
        try {
            SimpleFile.writeFile(FILE, CONTENTS);
        } catch (IOException e) {
            fail("Unexpected IOException");
            e.printStackTrace();
        }
        try {
            assertEquals(CONTENTS, SimpleFile.readFile(FILE));
        } catch (FileNotFoundException e) {
            fail("Unexpected FileNotFoundException");
            e.printStackTrace();
        }
    }

    @AfterEach
    void cleanUp() {
        File file = new File(FILE);
        file.delete();
    }
}