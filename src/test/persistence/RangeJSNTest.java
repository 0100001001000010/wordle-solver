package persistence;

import model.Guess;
import model.Range;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RangeJSNTest {
    @Test
    void testBothUnknown() {
        Range expected = new Range();
        Range actual = RangeJSN.fromJSNtoRange(
                RangeJSN.rangeToJSN(expected)
        );

        assertEquals(expected, actual);
    }

    @Test
    void testMinknown() {
        Range expected = new Range();
        expected.setMin(1);
        Range actual = RangeJSN.fromJSNtoRange(
                RangeJSN.rangeToJSN(expected)
        );

        assertEquals(expected, actual);
    }

    @Test
    void testMaxKnown() {
        Range expected = new Range();
        expected.setMax(1);
        Range actual = RangeJSN.fromJSNtoRange(
                RangeJSN.rangeToJSN(expected)
        );

        assertEquals(expected, actual);
    }

    @Test
    void testBothKnown() {
        Range expected = new Range();
        expected.setMin(1);
        expected.setMax(2);
        Range actual = RangeJSN.fromJSNtoRange(
                RangeJSN.rangeToJSN(expected)
        );

        assertEquals(expected, actual);
    }
}
