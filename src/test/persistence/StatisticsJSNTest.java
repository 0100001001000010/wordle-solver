package persistence;

import model.Statistics;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class StatisticsJSNTest {
    private static final int ATTEMPTS = 6;
    private Statistics statistics;
    
    @BeforeEach
    void setup() {
        Map<Integer, Integer> winMap = new HashMap<Integer, Integer>();
        for (int key = 1; key <= ATTEMPTS; key++) {
            winMap.put(key, key * key);
        }
        statistics = new Statistics(winMap, ATTEMPTS + 1);
    }
    
    @Test
    void persistenceTest() {
        Statistics expected = statistics;
        try {
            StatisticsJSN.writeStatistics(statistics);
        } catch (IOException e) {
            fail("Unexpected IOException thrown");
            e.printStackTrace();
        }
        Statistics actual = null;
        try {
            actual = StatisticsJSN.loadStatistics();
        } catch (FileNotFoundException e) {
            fail("Unexpected FileNotFoundException thrown");
            e.printStackTrace();
        }
        assertEquals(expected, actual);
    }

    @AfterEach
    void cleanUp() {
        File file = new File(StatisticsJSN.PATH);
        file.delete();
    }
}
