package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class StatisticsTest {
    private final int ATTEMPTS = 6;
    private Statistics statistics;

    @BeforeEach
    void setup() {
        Map<Integer, Integer> winMap = new HashMap<Integer, Integer>();
        for (int key = 1; key <= ATTEMPTS; key++) {
            winMap.put(key, key * key);
        }
        statistics = new Statistics(winMap, ATTEMPTS + 1);
    }

    @Test
    void testInitializationConstructor() {
        for (int attempts = 1; attempts <= 10; attempts++) {
            statistics = new Statistics(attempts);
            assertEquals(0, statistics.getLosses());
            Map<Integer, Integer> expectedWinMap = new HashMap<Integer, Integer>();
            for (int key = 1; key <= attempts; key++) {
                expectedWinMap.put(key, 0);
            }
            Map<Integer, Integer> actualWinMap = statistics.getInternalWinMap();
            assertEquals(expectedWinMap, actualWinMap);
        }
    }

    @Test
    void testDataLoadingConstructor() {
        final int attempts = 10;
        Map<Integer, Integer> expectedWins = new HashMap<Integer, Integer>();
        for (int key = 1; key <= attempts; key++) {
            expectedWins.put(key, key * key);
        }
        int expectedLosses = 15;

        statistics = new Statistics(expectedWins, expectedLosses);

        assertEquals(expectedWins, statistics.getInternalWinMap());
        assertEquals(expectedLosses, statistics.getLosses());
    }

    @Test
    void testGetWinRateOfAllWins() {
        for (int attempts = 1; attempts <= ATTEMPTS; attempts++) {
            assertEquals((double) attempts * attempts / statistics.getTotalWins(),
                    statistics.getWinRateOfAllWins(attempts));
        }
    }

    @Test
    void testGetWinRateOfAllGamesNumber() {
        for (int attempts = 1; attempts <= ATTEMPTS; attempts++) {
            assertEquals((double) attempts * attempts / statistics.getTotalGames(),
                    statistics.getWinRateOfAllGames(attempts));
        }
    }

    @Test
    void testGetWinRateOfAllGamesNoArg() {
        assertEquals((double) statistics.getTotalWins() / statistics.getTotalGames(),
                    statistics.getWinRateOfAllGames());
    }

    @Test
    void testGetLossRateOfAllGames() {
        assertEquals((double) statistics.getLosses() / statistics.getTotalGames(),
                statistics.getLossRateOfAllGames());
    }

    @Test
    void testGetTotalWins() {
        int expected = 0;
        for (int key: statistics.getInternalWinMap().keySet()) {
            expected += key * key;
        }
        assertEquals(expected, statistics.getTotalWins());
    }

    @Test
    void testGetTotalGames() {
        assertEquals(statistics.getTotalWins() + statistics.getLosses(), statistics.getTotalGames());
    }

    @Test
    void testAverageWinningGuesses() {
        double expected = 0;
        for (int i = 1; i <= ATTEMPTS; i++) {
            expected += i * statistics.getWinRateOfAllWins(i);
        }
        assertEquals(expected, statistics.getAverageWinningGuess());
    }

    @Test
    void testAddWin() {
        Map<Integer, Integer> expectedWinMap = new HashMap<Integer, Integer>(statistics.getInternalWinMap());
        expectedWinMap.put(1, expectedWinMap.get(1) + 1);
        statistics.addWin(1);
        assertEquals(expectedWinMap, statistics.getInternalWinMap());
    }

    @Test
    void testAddLoss() {
        int expectedLosses = statistics.getLosses() + 1;
        statistics.addLoss();
        assertEquals(expectedLosses, statistics.getLosses());
    }

    @Test
    void testEqualsNotObject() {
        assertFalse(statistics.equals("string"));
    }

    @Test
    void testEqualsWinMapFLossesF() {
        Statistics statistics2 = new Statistics(
                new HashMap<Integer, Integer>(statistics.getInternalWinMap()),
                statistics.getLosses()
        );
        statistics2.addWin(1);
        statistics2.addLoss();
        assertFalse(statistics.equals(statistics2));
    }

    @Test
    void testEqualsWinMapFLossesT() {
        Statistics statistics2 = new Statistics(
                new HashMap<Integer, Integer>(statistics.getInternalWinMap()),
                statistics.getLosses()
        );
        statistics2.addWin(1);
        assertFalse(statistics.equals(statistics2));
    }

    @Test
    void testEqualsWinMapTLossesF() {
        Statistics statistics2 = new Statistics(
                new HashMap<Integer, Integer>(statistics.getInternalWinMap()),
                statistics.getLosses()
        );
        statistics2.addLoss();
        assertFalse(statistics.equals(statistics2));
    }

    @Test
    void testEqualsWinMapTLossesT() {
        Statistics statistics2 = new Statistics(
                new HashMap<Integer, Integer>(statistics.getInternalWinMap()),
                statistics.getLosses()
        );
        assertTrue(statistics.equals(statistics2));
    }
}