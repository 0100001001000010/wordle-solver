package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangeTest {
    public final int TEST_MIN = -10;
    public final int TEST_MAX = 10;

    Range range;

    @BeforeEach
    void beforeEach(){
        this.range = new Range();
    }

    @Test
    void testConstructor(){
        assertFalse(range.minIsKnown());
        assertFalse(range.maxIsKnown());
    }

    @Test
    void testInRangeBothUnknown() {
        assertTrue(range.inRange(TEST_MIN - 1));
        assertTrue(range.inRange(TEST_MIN ));
        assertTrue(range.inRange(TEST_MIN + 1));

        assertTrue(range.inRange(TEST_MAX - 1));
        assertTrue(range.inRange(TEST_MAX ));
        assertTrue(range.inRange(TEST_MAX + 1));
    }

    @Test
    void testInRangeMinKnown() {
        range.setMin(this.TEST_MIN);

        assertFalse(range.inRange(TEST_MIN - 1));
        assertTrue(range.inRange(TEST_MIN ));
        assertTrue(range.inRange(TEST_MIN + 1));

        assertTrue(range.inRange(TEST_MAX - 1));
        assertTrue(range.inRange(TEST_MAX ));
        assertTrue(range.inRange(TEST_MAX + 1));
    }

    @Test
    void testInRangeMaxKnown() {
        range.setMax(this.TEST_MAX);

        assertTrue(range.inRange(TEST_MIN - 1));
        assertTrue(range.inRange(TEST_MIN ));
        assertTrue(range.inRange(TEST_MIN + 1));

        assertTrue(range.inRange(TEST_MAX - 1));
        assertTrue(range.inRange(TEST_MAX ));
        assertFalse(range.inRange(TEST_MAX + 1));
    }

    @Test
    void testInRangeBothKnown() {
        range.setMin(this.TEST_MIN);
        range.setMax(this.TEST_MAX);

        assertFalse(range.inRange(TEST_MIN - 1));
        assertTrue(range.inRange(TEST_MIN ));
        assertTrue(range.inRange(TEST_MIN + 1));

        assertTrue(range.inRange(TEST_MAX - 1));
        assertTrue(range.inRange(TEST_MAX ));
        assertFalse(range.inRange(TEST_MAX + 1));
    }

    @Test
    void testClone() {
        Range clone = range.clone();
        assertEquals(range, clone);
    }

    @Test
    void testEqual() {
        Range equal = range.clone();
        Range notEqual = range.clone();
        notEqual.setMin(0);

        assertTrue(range.equals(equal));
        assertFalse(range.equals(notEqual));
    }

    @Test
    //Setters are normally too trivial to be worth testing, but these ones also set other properties.
    void testSetMin() {
        range.setMin(this.TEST_MIN);
        assertEquals(this.TEST_MIN, range.getMin());
        assertTrue(range.minIsKnown());
    }

    @Test
    void testClearMin() {
        range.setMin(this.TEST_MIN);
        range.clearMin();
        assertFalse(range.minIsKnown());
    }

    @Test
    void testSetMax() {
        range.setMax(this.TEST_MAX);
        assertEquals(this.TEST_MAX, range.getMax());
        assertTrue(range.maxIsKnown());
    }

    @Test
    void testClearMax() {
        range.setMax(this.TEST_MAX);
        range.clearMax();
        assertFalse(range.maxIsKnown());
    }

    @Test
    void testEqualsNotRange() {
        assertFalse(range.equals("string"));
    }

    @Test
    /*
#     # #######  #####     #    ####### #######  #####  #######
##   ## #       #     #   # #      #    #       #     #    #
# # # # #       #        #   #     #    #       #          #
#  #  # #####   #  #### #     #    #    #####    #####     #
#     # #       #     # #######    #    #             #    #
#     # #       #     # #     #    #    #       #     #    #
#     # #######  #####  #     #    #    #######  #####     #
     */
    void testEquals() {
        final boolean[] bools = {false, true};
        for (boolean minKnownEq: bools) {
            for (boolean maxKnownEq: bools) {
                for (boolean minKnown: bools) {
                    for (boolean minEq: bools) {
                        for (boolean maxKnown: bools) {
                            for (boolean maxEq: bools) {
                                Range r1 = new Range();
                                Range r2 = new Range();

                                if (minEq) {
                                    r1.setMin(0);
                                    r2.setMin(0);
                                } else {
                                    r1.setMin(0);
                                    r2.setMin(1);
                                }

                                if (maxEq) {
                                    r1.setMax(0);
                                    r2.setMax(0);
                                } else {
                                    r1.setMax(0);
                                    r2.setMax(1);
                                }

                                if (minKnownEq) {
                                    if (minKnown) {
                                        //Do nothing, as we've already explicitly set the minimums, making them known
                                    } else {
                                        r1.clearMin();
                                        r2.clearMin();
                                    }
                                } else {
                                    if (minKnown) {
                                        r2.clearMin();
                                    } else {
                                        r1.clearMin();
                                    }
                                }

                                if (maxKnownEq) {
                                    if (maxKnown) {
                                        //Do nothaxg, as we've already explicitly set the maximums, makaxg them known
                                    } else {
                                        r1.clearMax();
                                        r2.clearMax();
                                    }
                                } else {
                                    if (maxKnown) {
                                        r2.clearMax();
                                    } else {
                                        r1.clearMax();
                                    }
                                }

                                assertEquals(
                                        minKnownEq && maxKnownEq
                                        && (!minKnown || minEq)
                                        && (!maxKnown || maxEq),
                                        r1.equals(r2)
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}