package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import persistence.WordList;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class GuessTest {
    private static final String DEFAULT_WORD = "aaaaa";

    private List<Integer> hintsWrong;
    private List<Integer> hintsCorrect;
    private Map<Character, Range> initialCharacterTotals;
    private List<List<Character>> initialAllowedLetters;
    private List<String> initialPossibleWords;
    private Guess guess;    //Should be set to a wrong guess initially

    @BeforeEach
    // Doing some initialization as a beforeEach method rather than a beforeAll method so we can modify the objects
    // without creating deep copies, while also not worrying about creating future tests.
    void beforeEach() {
        try {
            WordList.loadWords();
        } catch (IOException e) {
            fail("Unexpected IOException");
            e.printStackTrace();
        }
        this.hintsWrong = new ArrayList<Integer>();
        this.hintsCorrect = new ArrayList<Integer>();
        for(int i = 0; i < 5; i++){
            this.hintsWrong.add(Guess.HINT_NOT_IN_WORD);
            this.hintsCorrect.add(Guess.HINT_CORRECT);
        }

        this.initialCharacterTotals = new HashMap<Character, Range>();
        for(char letter: Guess.ALL_LETTERS){
            initialCharacterTotals.put(letter, new Range());
        }

        this.initialAllowedLetters = new ArrayList<List<Character>>();
        for(int i = 0; i < 5; i++){
            List<Character> listToAdd = new ArrayList<Character>();
            for(char letter: Guess.ALL_LETTERS) {
                listToAdd.add(letter);
            }
            initialAllowedLetters.add(listToAdd);
        }

        this.initialPossibleWords = WordList.getWordList();

        this.guess = new Guess(DEFAULT_WORD, this.hintsWrong);
    }

    @Test
    void testFirstGuessConstructor() {
        assertEquals(DEFAULT_WORD, this.guess.getWord());
        assertEquals(this.hintsWrong, this.guess.getHints());

        //I know this doesn't really make a copy. That's why I'm initializing it in a BeforeEach rather than a
        // BeforeAll. I'm just doing this because the original name no longer accurately describes what it is.
        Map<Character, Range> newCharacterTotals = this.initialCharacterTotals;
        newCharacterTotals.get('a').setMax(0);
        newCharacterTotals.get('a').setMin(0);

        List<List<Character>> newAllowedLetters = this.initialAllowedLetters;
        for(List sublist: newAllowedLetters){
            sublist.remove(new Character('a'));
        }

        List<String> newPossibleWords = this.initialPossibleWords;
        List<String> wordsToRemove = new ArrayList<String>();
        for(String word: newPossibleWords){
            if(word.contains("a")){
                wordsToRemove.add(word);
            }
        }
        newPossibleWords.removeAll(wordsToRemove);

        for (char letter: newCharacterTotals.keySet()) {
            assertEquals(newCharacterTotals.get(letter), guess.getLetterTotals().get(letter));
        }
        assertEquals(newAllowedLetters, guess.getAllowedLetters());
        assertEquals(newPossibleWords, guess.getNextWords());
    }

    @Test
    void testHintWrongPlace() {
        final int[] hintsArray = {Guess.HINT_CORRECT, Guess.HINT_CORRECT, Guess.HINT_CORRECT,
                Guess.HINT_WRONG_PLACE, Guess.HINT_WRONG_PLACE};
        List<Integer> hints = new ArrayList<Integer>();
        for (int element: hintsArray) {
            hints.add(element);
        }
        guess = new Guess("coats", hints);

        Map<Character, Range> newCharacterTotals = this.initialCharacterTotals;
        newCharacterTotals.get('c').setMin(1);
        newCharacterTotals.get('o').setMin(1);
        newCharacterTotals.get('a').setMin(1);
        newCharacterTotals.get('t').setMin(1);
        newCharacterTotals.get('s').setMin(1);

        List<List<Character>> newAllowedLetters = this.initialAllowedLetters;
        newAllowedLetters.get(0).clear();
        newAllowedLetters.get(0).add('c');
        newAllowedLetters.get(1).clear();
        newAllowedLetters.get(1).add('o');
        newAllowedLetters.get(2).clear();
        newAllowedLetters.get(2).add('a');
        newAllowedLetters.get(3).remove(new Character('t'));
        newAllowedLetters.get(4).remove(new Character('s'));

        List<String> newPossibleWords = new ArrayList<String>();
        newPossibleWords.add("coast");

        for (char letter: newCharacterTotals.keySet()) {
            assertEquals(newCharacterTotals.get(letter), guess.getLetterTotals().get(letter));
        }
        assertEquals(newAllowedLetters, guess.getAllowedLetters());
        assertEquals(newPossibleWords, guess.getNextWords());
    }

    @Test
    void testNextGuessConstructor() {
        Guess firstGuess = this.guess;
        Guess secondGuess = new Guess("bbbbb", this.hintsWrong, firstGuess.getLetterTotals(),
                firstGuess.getAllowedLetters(), firstGuess.getNextWords());

        Map<Character, Range> newCharacterTotals = firstGuess.getLetterTotals();
        newCharacterTotals.get('b').setMax(0);
        newCharacterTotals.get('b').setMin(0);

        List<List<Character>> newAllowedLetters = firstGuess.getAllowedLetters();
        for(List sublist: newAllowedLetters){
            sublist.remove(new Character('b'));
        }

        List<String> newPossibleWords = firstGuess.getNextWords();
        List<String> wordsToRemove = new ArrayList<String>();
        for(String word: newPossibleWords){
            if(word.contains("b")){
                wordsToRemove.add(word);
            }
        }
        newPossibleWords.removeAll(wordsToRemove);

        assertEquals(newCharacterTotals, secondGuess.getLetterTotals());
        assertEquals(newAllowedLetters, secondGuess.getAllowedLetters());
        assertEquals(newPossibleWords, secondGuess.getNextWords());
    }

    @Test
    void testMakeNextGuess() {
        Guess firstGuess = this.guess;
        Guess secondGuess = firstGuess.makeNextGuess("bbbbb", this.hintsWrong);

        Map<Character, Range> newCharacterTotals = firstGuess.getLetterTotals();
        newCharacterTotals.get('b').setMax(0);
        newCharacterTotals.get('b').setMin(0);

        List<List<Character>> newAllowedLetters = firstGuess.getAllowedLetters();
        for(List sublist: newAllowedLetters){
            sublist.remove(new Character('b'));
        }

        List<String> newPossibleWords = firstGuess.getNextWords();
        List<String> wordsToRemove = new ArrayList<String>();
        for(String word: newPossibleWords){
            if(word.contains("b")){
                wordsToRemove.add(word);
            }
        }
        newPossibleWords.removeAll(wordsToRemove);

        for (char letter: newCharacterTotals.keySet()) {
            assertEquals(newCharacterTotals.get(letter), secondGuess.getLetterTotals().get(letter));
        }
        assertEquals(newAllowedLetters, secondGuess.getAllowedLetters());
        assertEquals(newPossibleWords, secondGuess.getNextWords());
    }

    @Test
    void testIsCorrect() {
        Guess wrongGuess = this.guess;
        Guess correctGuess = new Guess("words", this.hintsCorrect);

        assertFalse(wrongGuess.isCorrect());
        assertTrue(correctGuess.isCorrect());
    }

    @Test
    void testEqualsNotGuess() {
        assertFalse(guess.equals("string"));
    }

    @Test
    void testEquals() {
        final boolean[] bools = {false, true};
        for (boolean wordEq: bools) {
            for (boolean hintsEq: bools) {
                for (boolean allowedLettersEq: bools) {
                    for (boolean possibleWordsEq: bools) {
                        Guess g1 = new Guess("aaaaa", hintsWrong);
                        Guess g2 = new Guess(wordEq ? "aaaaa" : "bbbbb", hintsEq ? hintsWrong : hintsCorrect);

                        if (allowedLettersEq) {
                            // Cleared lists are guaranteed to be the same
                            g1.getAllowedLetters().clear();
                            g2.getAllowedLetters().clear();
                        } else {
                            // Remove different characters to guarantee not the same
                            g1.getAllowedLetters().get(0).remove(new Character('y'));
                            g2.getAllowedLetters().get(1).remove(new Character('z'));
                        }

                        if (possibleWordsEq) {
                            g1.getNextWords().clear();
                            g2.getNextWords().clear();
                        } else {
                            g1.getNextWords().add("inequality generator 1");
                            g2.getNextWords().add("inequality generator 2");
                        }

                        if (!(wordEq && hintsEq && allowedLettersEq && possibleWordsEq)) {
                            assertFalse(g1.equals(g2));
                        } else {
                            boolean expected = true;
                            for (char letter: Guess.ALL_LETTERS) {
                                if (!g1.getLetterTotals().get(letter).equals(g2.getLetterTotals().get(letter))) {
                                    expected = false;
                                }
                            }
                            assertEquals(expected, g1.equals(g2));
                        }
                    }
                }
            }
        }
    }

    @Test
    void testEqualsDifferentWords() {
        assertFalse(new Guess("aaaaa", hintsWrong).equals(new Guess("bbbbb", hintsWrong)));
    }

    @Test
    void testDifferentLetterTotals() {
        Guess guess1 = new Guess("aaaaa", hintsWrong);
        Guess guess2 = new Guess("aaaaa", hintsWrong);
        guess2.getLetterTotals().get('a').setMin(15);

        assertFalse(guess1.equals(guess2));
    }
}