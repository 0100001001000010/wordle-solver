package model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import persistence.WordList;

import java.io.IOException;
import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GuessListTest {
    private final int TEST_MAX_GUESSES = 6; //The maximum number of guesses allowed in test GuessList
    private final String[] WRONG_GUESSES = {"aaaaa", "bbbbb", "ccccc", "ddddd", "eeeee"}; //Wrong guesses for testing

    private GuessList guessList;            //Guess list

    //Used to create guesses when a populated GuessList is needed
    private List<Integer> hintsWrong;       //A list of hints that will result in a guess being wrong
    private List<Integer> hintsCorrect;     //A list of hints that will result in a guess being correct
    private List<Guess> guesses;            //A list of 5 wrong guesses, followed by one correct guess

    @BeforeEach
    void beforeEach() {
        try {
            WordList.loadWords();
        } catch (IOException e) {
            fail("Unexpected IOException thrown!");
            e.printStackTrace();
        }
        this.hintsWrong = new ArrayList<Integer>();
        this.hintsCorrect = new ArrayList<Integer>();
        for(int i = 0; i < 5; i++){
            this.hintsWrong.add(Guess.HINT_NOT_IN_WORD);
            this.hintsCorrect.add(Guess.HINT_CORRECT);
        }

        this.guesses = new ArrayList<Guess>();
        for(String word: this.WRONG_GUESSES ){
            this.guesses.add(new Guess(word, this.hintsWrong));
        }
        this.guesses.add(new Guess("words", this.hintsCorrect));
        this.guessList = new GuessList(this.TEST_MAX_GUESSES);
    }

    @Test
    void testConstructor() {
        assertEquals(new ArrayList<Guess>(), this.guessList.getInternalList());
        assertEquals(GuessList.IN_PROGRESS, this.guessList.getState());
        assertEquals(this.TEST_MAX_GUESSES, this.guessList.getLoseAfter());
    }

    @Test
    //Test that Add adds to the internal list. Does not check whether it marks the game as won or lost
    void testAdd() {
        Guess guess = new Guess("aaaaa", hintsWrong);
        List<Guess> expected = new ArrayList<Guess>(guessList.getInternalList());
        expected.add(guess);
        guessList.add(guess);
        assertEquals(expected, guessList.getInternalList());
    }

    @Test
    void testGet() {
        for (Guess guess: guesses) {
            guessList.add(guess);
        }
        for (int i = 0; i < guesses.size(); i++) {
            assertEquals(guessList.getInternalList().get(i), guessList.get(i));
        }
    }

    @Test
    void testGetNumGuesses() {
        assertEquals(0, guessList.getNumGuesses());
        for (int i = 0; i < guesses.size(); i++) {
            guessList.add(guesses.get(i));
            assertEquals(i + 1, guessList.getNumGuesses());
        }
    }

    @Test
    void testGetLastGuess() {
        for(Guess guess: guesses) {
            this.guessList.add(guess);
            assertEquals(guess, guessList.getLastGuess());
        }
    }

    @Test
    void testWinByAdd() {
        assertFalse(this.guessList.gameWon());
        this.guessList.add(new Guess("words", this.hintsCorrect));
        assertTrue(this.guessList.gameWon());
        assertEquals(GuessList.WON, this.guessList.getState());
    }

    @Test
    void testLoseByAdd() {
        assertFalse(this.guessList.gameLost());
        for(int i = 0; i < this.TEST_MAX_GUESSES; i++) {
            this.guessList.add(new Guess("aaaaa", this.hintsWrong));
        }
        assertTrue(this.guessList.gameLost());
        assertEquals(GuessList.LOST, this.guessList.getState());
    }

    @Test
    // Test that if loseAfter is zero, adding a wrong guess doesn't trigger a loss
    void dontLoseByAdd(){
        this.guessList = new GuessList(0);

        assertFalse(this.guessList.gameLost());
        this.guessList.add(new Guess("aaaaa", this.hintsWrong));
        assertFalse(this.guessList.gameLost());
        assertEquals(GuessList.IN_PROGRESS, this.guessList.getState());
    }

    @Test
    void testwinGame() {
        assertFalse(this.guessList.gameWon());
        this.guessList.winGame();
        assertTrue(this.guessList.gameWon());
        assertEquals(GuessList.WON, this.guessList.getState());
    }

    @Test
    void testloseGame() {
        assertFalse(this.guessList.gameLost());
        this.guessList.loseGame();
        assertTrue(this.guessList.gameLost());
        assertEquals(GuessList.LOST, this.guessList.getState());
    }

    @Test
    void testGameInProgress() {
        this.guessList.setState(GuessList.IN_PROGRESS);
        assertTrue(this.guessList.gameInProgress());
        this.guessList.setState(GuessList.WON);
        assertFalse(this.guessList.gameInProgress());
        this.guessList.setState(GuessList.LOST);
        assertFalse(this.guessList.gameInProgress());
    }

    @Test
    void testGameWon() {
        this.guessList.setState(GuessList.IN_PROGRESS);
        assertFalse(this.guessList.gameWon());
        this.guessList.setState(GuessList.WON);
        assertTrue(this.guessList.gameWon());
        this.guessList.setState(GuessList.LOST);
        assertFalse(this.guessList.gameWon());
    }

    @Test
    void testGameLost() {
        this.guessList.setState(GuessList.IN_PROGRESS);
        assertFalse(this.guessList.gameLost());
        this.guessList.setState(GuessList.WON);
        assertFalse(this.guessList.gameLost());
        this.guessList.setState(GuessList.LOST);
        assertTrue(this.guessList.gameLost());
    }

    @Test
    void testEqualsNotGuessList() {
        assertFalse(guessList.equals("string"));
    }

    @Test
    void testEuqals() {
        final boolean[] bools = {false, true};
        for (boolean stateEq: bools) {
            for (boolean loseAfterEq: bools) {
                for (boolean guessListEq: bools) {
                    GuessList gl1 = new GuessList(6);
                    GuessList gl2 = new GuessList(loseAfterEq ? 6 : 7);
                    if (!stateEq) {
                        gl2.winGame();
                    }
                    if (!guessListEq) {
                        gl2.add(new Guess("abcde", hintsWrong));
                    }

                    assertEquals(stateEq && loseAfterEq && guessListEq, gl1.equals(gl2));
                }
            }
        }
    }
}