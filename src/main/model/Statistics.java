package model;

import java.util.HashMap;
import java.util.Map;

// Store information about the total number of wins (for each number of attempts) and losses so far
public class Statistics {
    //Using a map between Integer and Integer because I want 1-based indexing here, not 0-based that lists use.
    private Map<Integer, Integer> wins;
    private int losses;

    //REQUIRES: maxAttempts > 0
    //EFFECTS: Creates a new Statistics object with up to maxAttempts guesses allowed, no wins, and no losses
    public Statistics(int maxAttempts) {
        Map<Integer, Integer> wins = new HashMap<Integer, Integer>();
        for (int i = 1; i <= maxAttempts; i++) {
            wins.put(i, 0);
        }
        this.wins = wins;
        this.losses = 0;
    }

    //REQUIRES: wins.keySet().size() > 0, every value in wins >= 0, losses >= 0
    //EFFECTS: Create a new Statistics object with the given number of wins in each number of guesses and number of
    //         losses
    public Statistics(Map<Integer, Integer> wins, int losses) {
        this.wins = wins;
        this.losses = losses;
    }

    //REQUIRES: 1 <= attempts <= wins.keySet().size()
    //MODIFIES: this
    //EFFECTS: Records a win in the given number of attempts
    public void addWin(int attempts) {
        this.wins.put(attempts, this.wins.get(attempts) + 1);
    }

    //MODIFIES: this
    //EFFECTS: Records a loss
    public void addLoss() {
        this.losses++;
    }

    //EFFECTS: Returns the total number of wins
    public int getTotalWins() {
        int sum = 0;
        for (int value: wins.values()) {
            sum += value;
        }
        return sum;
    }

    //EFFECTS: Returns the total number of games played
    public int getTotalGames() {
        return getTotalWins() + losses;
    }

    //EFFECTS: wins in x attempts / total wins
    public double getWinRateOfAllWins(int attempts) {
        return (double) wins.get(attempts) / getTotalWins();
    }

    //EFFECTS: wins in x attempts / total games
    public double getWinRateOfAllGames(int attempts) {
        return (double) wins.get(attempts) / getTotalGames();
    }

    //EFFECTS: total wins / total games
    public double getWinRateOfAllGames() {
        return (double) getTotalWins() / getTotalGames();
    }

    //EFFECTS: total losses / total games
    public double getLossRateOfAllGames() {
        return (double) losses / getTotalGames();
    }

    //EFFECTS: Returns the number of games that have been lost
    public int getLosses() {
        return losses;
    }

    //EFFECTS: Returns the internal
    public Map<Integer, Integer> getInternalWinMap() {
        return wins;
    }

    //EFFECTS: Uses a weighted average to calculate the average number of guesses of games that were won
    public double getAverageWinningGuess() {
        double result = 0;
        for (int attempts: wins.keySet()) {
            result += attempts * getWinRateOfAllWins(attempts);
        }
        return result;
    }

    //TESTING METHOD

    //EFFECTS: Returns whether the supplied object is an equivalent Statistics object
    //TODO also override hashcode()
    @Override
    public boolean equals(Object otherObj) {
        if (!(otherObj instanceof Statistics)) {
            return false;
        }
        Statistics other = (Statistics) otherObj;

        return this.wins.equals(other.getInternalWinMap()) && this.losses == other.getLosses();
    }
}
