package model;


import persistence.WordList;

import java.util.*;

/* Represents a guess that the user has made, including the word, hints given by the game, and information to take into
   account for future guesses: how many of each character allowed overall, which character allowed in each position,
   and words that haven't been eliminated yet.
 */
public class Guess {
    //TODO use an enum here
    public static final int HINT_NOT_IN_WORD = 0;           //The letter is not in the word
    public static final int HINT_WRONG_PLACE = 1;           //The letter is in the word, but not here
    public static final int HINT_CORRECT = 2;               //The letter is in the word in the correct place
    public static final char[] ALL_LETTERS = {              //Every letter
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
    };
    public static final int WORD_LENGTH = 5;                //The length of the words

    private String word;                                    //The actual word that was guessed
    private List<Integer> hints;                            //The list of hints
    private Map<Character, Range> letterTotals;            //A mapping between each guessed so far and minimum and
                                                            // maximum allowed quantities if known
    private List<List<Character>> allowedLetters;           //A mapping between letter position and what those letters
                                                            //are allowed to be
    private List<String> possibleWords;                     //A list of possible words

    //REQUIRES: word.length == WORD_LENGTH, hints.size == WORD_LENGTH
    //EFFECTS: Creates a new instance of the Guess object without any prior knowledge from other guesses
    public Guess(String word, List<Integer> hints) {
        this.word = word;
        this.hints = hints;

        this.letterTotals = new Hashtable<Character, Range>();
        for (char letter: ALL_LETTERS) {
            this.letterTotals.put(letter, new Range());
        }

        this.allowedLetters = new ArrayList<List<Character>>();
        for (int i = 0; i < WORD_LENGTH; i++) {
            // The other ways of converting an array to an ArrayList were throwing exceptions, so I had to do it this
            // way despite how suboptimal it is. And I really do need to create a new sublist every time because they
            // may be modified independently later.
            ArrayList<Character> allLetters = new ArrayList<Character>();
            for (char letter: Guess.ALL_LETTERS) {
                allLetters.add(letter);
            }
            this.allowedLetters.add(allLetters);
        }

        this.possibleWords = WordList.getWordList();

        this.updateAllowedLetters();
        this.updateLetterTotals();
        this.updatePossibleWords();
    }

    //REQUIRES: word.length == WORD_LENGTH, hints.size == WORD_LENGTH, one of the following:
    //            * letterTotals has a Range object for every letter of the alphabet in lowercase,
    //              allowedCharacters.size == WORD_LENGTH, allowedCharacters contains lists of lowercase letters,
    //              possibleWords contains a list of WORD_LENGTH letter lowercase words
    //            * letterTotals, allowedLetters, and possibleWords are empty, but this constructor is being called
    //              from the overloaded constructor, and it will initialize them to conform to the requirements.
    //EFFECTS: Creates a new instance of the Guess object with the given prior knowledge from other guesses, making a
    //         copy of characterTotals, allowedLetters, and possibleWords to avoid overwriting the originals
    //         Do further adjustment of characterTotals, allowedLetters, and possibleWords based on the word and hints
    public Guess(String word, List<Integer> hints, Map<Character, Range> letterTotals,
                 List<List<Character>> allowedLetters, List<String> possibleWords) {
        this.word = word;
        this.hints = hints;

        this.letterTotals = new HashMap<Character, Range>();
        for (char letter: letterTotals.keySet()) {
            this.letterTotals.put(letter, letterTotals.get(letter).clone());
        }

        this.allowedLetters = new ArrayList<List<Character>>();
        for (List<Character> sublist: allowedLetters) {
            List<Character> newList = new ArrayList<Character>();
            newList.addAll(sublist);
            this.allowedLetters.add(newList);
        }

        this.possibleWords = new ArrayList<String>();
        this.possibleWords.addAll(possibleWords);

        this.updateAllowedLetters();
        this.updateLetterTotals();
        this.updatePossibleWords();
    }

    //REQUIRES: Function is only called once per object, by the constructor.
    //MODIFIES: this
    //EFFECTS: Updates this.allowedLetters to only allow the current letter in positions where it is correct,
    //         and disallow the current letter in positions where it is either in the wrong place or not in the word.
    private void updateAllowedLetters() {
        for (int i = 0; i < Guess.WORD_LENGTH; i++) {
            if (this.hints.get(i) == Guess.HINT_CORRECT) {  //If the letter is correct, the letter must be this
                this.allowedLetters.set(i, new ArrayList<Character>());
                this.allowedLetters.get(i).add(this.word.charAt(i));
            } else {                                        //Otherwise, the letter must not be this
                this.allowedLetters.get(i).remove(new Character(this.word.charAt(i)));
            }
        }
    }

    //REQUIRES: Function io only called once per object, by the constructor.
    //MODIFIES: this
    //EFFECTS: Sets the totals for each letter in the word to the following:
    //           * minimum: larger of correct + wrong place and existing minimum if known
    //           * maximum: same as minimum if non-zero amount of not in word
    private void updateLetterTotals() {
        for (char letter: this.word.toCharArray()) {
            int correct = 0;
            int wrongPlace = 0;
            int notInWord = 0;
            for (int i = 0; i < Guess.WORD_LENGTH; i++) {
                if (this.word.charAt(i) == letter && this.hints.get(i) == Guess.HINT_CORRECT) {
                    correct++;
                } else if (this.word.charAt(i) == letter && this.hints.get(i) == Guess.HINT_WRONG_PLACE) {
                    wrongPlace++;
                } else if (this.word.charAt(i) == letter && this.hints.get(i) == Guess.HINT_NOT_IN_WORD) {
                    notInWord++;
                }
            }
            if (!this.letterTotals.get(letter).minIsKnown()
                    || correct + wrongPlace > this.letterTotals.get(letter).getMin()) {
                this.letterTotals.get(letter).setMin(correct + wrongPlace);
            }
            if (notInWord != 0) {
                this.letterTotals.get(letter).setMax(correct + wrongPlace);
            }
        }
    }

    //REQUIRES: Functions is only called once per object, by the constructor.
    //MODIFIES: this
    //EFFECTS: Filter the word list so only the words which match the allowed letters and letter totals rules remain
    private void updatePossibleWords() {
        List<String> wordsToRemove = new ArrayList<String>();
        for (String word: this.possibleWords) {
            for (int i = 0; i < Guess.WORD_LENGTH; i++) {
                //Eliminate the word if the character isn't allowed at this positon
                if (!this.allowedLetters.get(i).contains(word.charAt(i))) {
                    wordsToRemove.add(word);
                    break;
                }
            }
            for (char letter: Guess.ALL_LETTERS) {
                //Eliminate the word if this letter isn't present in an allowed quantity
                if (!(this.letterTotals.get(letter).inRange(word.length()
                        - word.replaceAll("" + letter, "").length()))) {
                    wordsToRemove.add(word);
                    break;
                }
            }
        }
        this.possibleWords.removeAll(wordsToRemove);
    }

    //REQUIRES: word.length == WORD_LENGTH, hints.size == WORD_LENGTH
    //EFFECTS: Returns a new Guess object, with given word and hints, but starting with existing knowledge
    public Guess makeNextGuess(String word, List<Integer> hints) {
        return new Guess(word, hints, this.letterTotals,
                this.allowedLetters, this.possibleWords);
    }

    //EFFECTS: Returns whether every character in the word is in the correct place: if this is true, the game is won.
    public boolean isCorrect() {
        for (int hint: this.hints) {
            if (hint != Guess.HINT_CORRECT) {
                return false;
            }
        }
        return true;
    }

    //EFFECTS: Return the word that was guessed
    public String getWord() {
        return word;
    }

    //EFFECTS: Return the list of hints
    public List<Integer> getHints() {
        return hints;
    }

    //EFFECTS: Returns the list of words that have not yet been eliminated
    public List<String> getNextWords() {
        return this.possibleWords;
    }

    //EFFECTS: Return the internal character totals
    public Map<Character, Range> getLetterTotals() {
        return letterTotals;
    }

    //EFFECTS: Return the list of allowed characters in each position
    public List<List<Character>> getAllowedLetters() {
        return allowedLetters;
    }

    //TESTING METHOD

    //EFFECTS: Return whether the provided object is an equivalent Guess object
    //TODO also override hashcode()
    @Override
    public boolean equals(Object otherObj) {
        if (!(otherObj instanceof Guess)) {
            return false;
        }
        Guess other = (Guess) otherObj;
        if (!(this.word.equals(other.getWord())
                && this.hints.equals(other.getHints())
                && this.allowedLetters.equals(other.getAllowedLetters())
                && this.possibleWords.equals(other.getNextWords()))) {
            return false;
        }
        for (char letter: Guess.ALL_LETTERS) {
            if (!letterTotals.get(letter).equals(other.getLetterTotals().get(letter))) {
                return false;
            }
        }
        return true;
    }
}
