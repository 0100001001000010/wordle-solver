package model;

import java.util.ArrayList;
import java.util.List;

// Represents the state of a game as a list of guesses and variable to indicate whether won, lost, or in progress
public class GuessList {
    public static final int IN_PROGRESS = 0;    //The word has not been guessed, and more guesses can be made
    public static final int WON = 1;            //The word has been guessed
    public static final int LOST = 2;           //The word has not been guessed, but no more guesses can be made

    private List<Guess> guessList;
    private int state;
    private int loseAfter;

    //REQUIRES: loseAfter >= 0
    //EFFECTS: Creates a new GuessList object with empty list of guesses and the specified maximum number of guesses.
    //         if loseAfter == 0, the game will never automatically end. Otherwise, the game will automatically end
    //         after that many guesses have been made.
    public GuessList(int loseAfter) {
        this.guessList = new ArrayList<Guess>();
        this.state = GuessList.IN_PROGRESS;
        this.loseAfter = loseAfter;
    }

    //REQUIRES: this.state == GuessList.IN_PROGRESS
    //MODIFIES: this
    //EFFECTS: Adds a guess to the guess list, changes the game state to lost if maximum number of guesses is reached
    //         and game has not been won, changes the game state to won if the guess is correct.
    public void add(Guess guess) {
        EventLog.getInstance().logEvent(new Event("Guessing " + guess.getWord()));
        this.guessList.add(guess);

        if (guess.isCorrect()) {
            this.winGame();
        } else if (loseAfter != 0 && this.getNumGuesses() >= this.loseAfter) {
            this.loseGame();
        }
    }

    //REQUIRES: 0 <= index < this.getNumGuesses()
    //EFFECTS: Returns the guess at the specified index
    public Guess get(int index) {
        return this.guessList.get(index);
    }

    //EFFECTS: Returns the number of guesses that have been made
    public int getNumGuesses() {
        return this.guessList.size();
    }

    //REQUIRES: this.guessList.size() > 0
    //EFFECTS: Returns the latest guess
    public Guess getLastGuess() {
        return this.guessList.get(this.guessList.size() - 1);
    }

    //REQUIRES: this.state == GuessList.IN_PROGRESS
    //MODIFIES: this
    //EFFECTS: Marks the game as won
    public void winGame() {
        this.state = GuessList.WON;
    }

    //REQUIRES: this.state == GuessList.IN_PROGRESS
    //MODIFIES: this
    //EFFECTS: Marks the game as lost
    public void loseGame() {
        this.state = GuessList.LOST;
    }

    //EFFECTS: Returns whether the game is in progress
    public boolean gameInProgress() {
        return this.state == GuessList.IN_PROGRESS;
    }

    //EFFECTS: Returns whether the game has been won
    public boolean gameWon() {
        return this.state == GuessList.WON;
    }

    //EFFECTS: Returns whether the game has been lost
    public boolean gameLost() {
        return this.state == GuessList.LOST;
    }

    //EFFECTS: Returns the internal list of guesses
    public List<Guess> getInternalList() {
        return this.guessList;
    }

    //EFFECTS: Return the actual game state field
    public int getState() {
        return this.state;
    }

    //EFFECTS: Return the field representing how many guesses to automatically lose after
    public int getLoseAfter() {
        return this.loseAfter;
    }

    //REQUIRES: state is GuessList.IN_PROGRESS, GuessList.WON, or GuessList.LOST
    //MODIFIES: this
    //EFFECTS: Sets the state to the desired value
    public void setState(int state) {
        this.state = state;
    }

    //EFFECTS: Checks whether a given object is an equivalent GuessList object
    //TODO also override hashcode()
    @Override
    public boolean equals(Object otherObj) {
        if (!(otherObj instanceof GuessList)) {
            return false;
        }
        GuessList other = (GuessList) otherObj;
        return this.state == other.getState()
                && this.loseAfter == other.getLoseAfter()
                && this.guessList.equals(other.getInternalList());
    }

    private String getHintUpdateLogString(int guessIndex, int letterIndex, int newHint) {
        if (newHint == Guess.HINT_CORRECT) {
            return String.format("Letter %s of guess %s said to be correct",
                    String.valueOf(letterIndex), String.valueOf(guessIndex));
        } else if (newHint == Guess.HINT_WRONG_PLACE) {
            return String.format("Letter %s of guess %s said to be in the wrong place",
                    String.valueOf(letterIndex), String.valueOf(guessIndex));
        } else {
            return String.format("Letter %s of guess %s said to not be in word",
                    String.valueOf(letterIndex), String.valueOf(guessIndex));
        }
    }

    //REQUIRES: 0 <= guessIndex < getNumGuesses(), 0 <= letterIndex < Guess.WORD_LENGTH,
    //          newHint is Guess.HINT_NOT_IN_WORD, Guess.HINT_WRONG_PLACE, or Guess.HINT_CORRECT = 2;
    //MODIFIES: this
    //EFFECTS: Set the hint for letter letterIndex of guess guessIndex to newHint, then regenerate subsequent guesses.
    public void updateHint(int guessIndex, int letterIndex, int newHint) {
        String word = guessList.get(guessIndex).getWord();
        List<Integer> hints = new ArrayList<>(guessList.get(guessIndex).getHints());
        hints.set(letterIndex, newHint);
        EventLog.getInstance().logEvent(new Event(getHintUpdateLogString(guessIndex, letterIndex, newHint)));

        if (guessIndex == 0) {
            getInternalList().set(0, new Guess(word, hints));
        } else {
            Guess lastGuess = getInternalList().get(guessIndex - 1);
            getInternalList().set(guessIndex, new Guess(word, new ArrayList<>(hints),
                    lastGuess.getLetterTotals(), lastGuess.getAllowedLetters(), lastGuess.getNextWords()));
        }

        //Update the later guesses to be based on the updated guess, rather than the old one
        for (int i = guessIndex + 1; i < getNumGuesses(); i++) {
            Guess lastGuess = getInternalList().get(i - 1);
            Guess thisGuessOld = getInternalList().get(i);
            getInternalList().set(i, new Guess(thisGuessOld.getWord(), thisGuessOld.getHints(),
                    lastGuess.getLetterTotals(), lastGuess.getAllowedLetters(), lastGuess.getNextWords()));
        }

        //Force the game back to being in progress, just in case incomplete hints caused it to be flagged as
        //being lost.
        setState(GuessList.IN_PROGRESS);
    }
}
