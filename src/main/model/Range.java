package model;

// Represents a range of values where the minimum and maximum both might or might not be known
public class Range implements Cloneable {
    private boolean minKnown;   //Whether the minimum is known
    private boolean maxKnown;   //Whether the maximum is known
    private int min;            //The minimum (inclusive), if it is known
    private int max;            //The maximum (inclusive), if it is known

    //EFFECTS: Create a new instance of Range where both minimum and maximum are unknown
    public Range() {
        minKnown = false;
        maxKnown = false;
    }

    //EFFECTS: Returns whether the number is within the range of acceptable values. A number is in the range if:
    //           * The minimum is unknown or number >= min, and
    //           * The maximum is unknown or number <= max
    public boolean inRange(int number) {
        return (!this.minKnown || number >= this.min)
                && (!this.maxKnown || number <= this.max);
    }

    //EFFECTS: Makes a deep copy of the Range object
    @Override
    public Range clone() {
        Range newRange = new Range();
        if (this.minKnown) {
            newRange.setMin(this.getMin());
        }
        if (this.maxKnown) {
            newRange.setMax(this.getMax());
        }

        return newRange;
    }

    //EFFECTS: Checks whether a given object is an equivalent Range object
    //TODO also override hashcode()
    @Override
    public boolean equals(Object otherObj) {
        if (!(otherObj instanceof Range)) {
            return false;
        }
        Range other = (Range) otherObj;
        return this.minIsKnown() == other.minIsKnown() && this.maxIsKnown() == other.maxIsKnown()
                && (!this.minIsKnown() || (this.getMin() == other.getMin()))
                && (!this.maxIsKnown() || (this.getMax() == other.getMax()));
    }

    //REQUIRES: The minimum is known
    //EFFECTS: Returns the minimum
    public int getMin() {
        return min;
    }

    //EFFECTS: Sets the minimum and declares it known
    public void setMin(int min) {
        this.minKnown = true;
        this.min = min;
    }

    //EFFECTS: Declare the minimum to be unknown
    public void clearMin() {
        this.minKnown = false;
    }

    //REQUIRES: The maximum is known
    //EFFECTS: Returns the maximum
    public int getMax() {
        return max;
    }

    //EFFECTS: Sets the maximum and declares it known
    public void setMax(int max) {
        this.maxKnown = true;
        this.max = max;
    }

    //EFFECTS: Declare the maximum to be unknown
    public void clearMax() {
        this.maxKnown = false;
    }

    //EFFECTS: Returns whether the minimum is known
    public boolean minIsKnown() {
        return this.minKnown;
    }

    //EFFECTS: Returns whether the maximum is known
    public boolean maxIsKnown() {
        return this.maxKnown;
    }
}
