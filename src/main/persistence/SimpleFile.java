package persistence;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

//Wrap the multiple lines for reading and writing text files into easy to use one-liners
public class SimpleFile {

    //EFFECTS: Returns the contents of the file at path, and throws FileNotFoundException if it doesn't exist
    public static String readFile(String path) throws FileNotFoundException {
        File file = new File(path);
        Scanner scanner = new Scanner(file);
        String result = "";
        while (scanner.hasNext()) {
            result += scanner.next();
            result += '\n';
        }
        scanner.close();
        return result;
    }

    //EFFECTS: Writes the specified contents to the specified path, throws IOException if that fails
    public static void writeFile(String path, String contents) throws IOException {
        File file = new File(path);
        FileWriter writer = new FileWriter(file);
        writer.write(contents);
        writer.close();
    }
}
