package persistence;

import org.json.JSONArray;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// Handles the loading and modification of the word list
public class WordList {
    public static final String INITIAL_WORD_LIST_PATH = "data/initialWordList.txt";
    public static final String SAVED_WORD_LIST_PATH = "data/wordList.json";

    private static List<String> wordList;

    //MODIFIES: WordList
    //EFFECTS: Tries to call loadSaved(), and calls loadInitial if that fails.
    public static List<String> loadWords() throws FileNotFoundException, IOException {
        try {
            return loadSaved();
        } catch (FileNotFoundException e) {
            return loadInitial();
        }
    }

    //MODIFIES: WordList
    //EFFECTS: Reads saved word list from filesystem, saves static reference, returns it.
    //         Throws FileNotFoundException if this list has not been created yet
    public static List<String> loadSaved() throws FileNotFoundException {
        JSONArray json = new JSONArray(SimpleFile.readFile(SAVED_WORD_LIST_PATH));
        wordList = new ArrayList<String>();
        for (int i = 0; i < json.length(); i++) {
            wordList.add(json.getString(i));
        }
        return wordList;
    }

    //MODIFIES: WordList
    //EFFECTS: Loads the default word list, saves static reference, saves to filesystem, returns default word list
    public static List<String> loadInitial() throws FileNotFoundException, IOException {
        wordList = new ArrayList<String>();
        Scanner scanner = new Scanner(SimpleFile.readFile(INITIAL_WORD_LIST_PATH));
        while (scanner.hasNextLine()) {
            wordList.add(scanner.nextLine());
        }

        SimpleFile.writeFile(SAVED_WORD_LIST_PATH, new JSONArray(wordList).toString());
        return wordList;
    }

    //REQUIRES: The word to be removed is in the word list
    //MODIFIES: WordList
    //EFFECTS: Deletes a word from both the active word list and the word list on the filesystem. If writing failed,
    //         throw IOException
    public static void deleteWord(String word) throws IOException {
        wordList.remove(word);
        SimpleFile.writeFile(SAVED_WORD_LIST_PATH, new JSONArray(wordList).toString());
    }

    //REQUIRES: The word list has been loaded
    //EFFECTS: Return a cached word list
    public static List<String> getWordList() {
        return new ArrayList<String>(wordList);
    }
}