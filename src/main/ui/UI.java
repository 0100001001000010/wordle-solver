package ui;

import model.GuessList;
import model.Statistics;
import java.util.Random;

//Contains the common features of all user interfaces.
//Originally, this was supposed to be most of the functionality, but it ends up being just a few constants and fields
//because of how different GUI and console interfaces are.
//This has to be an abstract class and not an interface because I want to use protected, and interface only allows
//public.
public abstract class UI {
    protected static final int LOSE_AFTER = 6;
    protected static final String MAIN_MENU_PROMPT = "Select an option: ";
    protected static final String MAIN_MENU_PROMPT_NO_SAVED = "There is no saved guess list. Select another option: ";
    protected static final String SAVE_FAILED_PROMPT = "Saving has failed. Do you still want to quit? ";
    protected static Random random;
    protected static Statistics statistics;
    protected static GuessList guessList;
}
