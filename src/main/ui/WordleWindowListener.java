package ui;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import static ui.Graphical.controlButtonHandler;

//Implements WindowListener to listen for the window being closed by the user and display appropriate prompts.
public class WordleWindowListener implements WindowListener {
    //EFFECTS: Do nothing, but I have to have this here because I have to implement WindowListener
    public void windowOpened(WindowEvent e) {}

    //EFFECTS: Do nothing, but I have to have this here because I have to implement WindowListener
    public void windowIconified(WindowEvent e) {}

    //EFFECTS: Do nothing, but I have to have this here because I have to implement WindowListener
    public void windowDeiconified(WindowEvent e) {}

    //EFFECTS: Do nothing, but I have to have this here because I have to implement WindowListener
    public void windowActivated(WindowEvent e) {}

    //EFFECTS: Do nothing, but I have to have this here because I have to implement WindowListener
    public void windowDeactivated(WindowEvent e) {}

    //EFFECTS: Do nothing, but I have to have this here because I have to implement WindowListener
    public void windowClosed(WindowEvent e) {}

    //EFFECTS: As the user attempts to close the window through the window manager, invoke the standard prompt as if
    //         they pressed the quit button.
    public void windowClosing(WindowEvent e) {
        controlButtonHandler.quit();
    }
}