package ui;

import model.Guess;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static ui.Graphical.gameEnded;
import static ui.Graphical.guessPanelButtons;
import static ui.UI.LOSE_AFTER;
import static ui.UI.guessList;

//Acts as a listener for the hint buttons, and contains code to update the color of the hint buttons on demand
public class HintAreaUpdater implements ActionListener {

    //REQUIRES: e is a string in the form of two integers separated by a colon.
    //          0 <= first integer < LOSE_AFTER
    //          0 <= second integer < Guess.WORD_LENGTH
    //MODIFIES: this
    //EFFECTS: Update the appropriate hint and internal information based on those hints according to which hint
    //         button was pressed
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        int colonPos = command.indexOf(':');
        int wordNum = Integer.parseInt(command.substring(0, colonPos));
        int letterNum = Integer.parseInt(command.substring(colonPos + 1));

        //Don't do anything if the clicked button does not correspond to a guess.
        if (wordNum >= guessList.getNumGuesses()) {
            return;
        }

        //TODO update to use enum length when hints enum gets introduced
        int newHint = (guessList.get(wordNum).getHints().get(letterNum) + 1) % 3;

        guessList.updateHint(wordNum, letterNum, newHint);

        updateButtons();
    }

    //MODIFIES: this
    //EFFECTS: Changes the colors and labels of the hint buttons to match the internal state
    public void updateButtons() {
        for (int i = 0; i < LOSE_AFTER; i++) {
            for (int j = 0; j < Guess.WORD_LENGTH; j++) {
                JButton button = guessPanelButtons.get(i).get(j);
                if (i < guessList.getNumGuesses()) {
                    button.setText(String.valueOf(guessList.get(i).getWord().charAt(j)).toUpperCase());
                    Integer integer = guessList.get(i).getHints().get(j);
                    if (integer == Guess.HINT_CORRECT) {
                        button.setBackground(Color.GREEN);
                    } else if (integer == Guess.HINT_WRONG_PLACE) {
                        button.setBackground(Color.YELLOW);
                    } else {
                        button.setBackground(Color.GRAY);
                    }
                } else {
                    button.setText(" ");
                    button.setBackground(Color.gray);
                }
            }
        }
        //We only end the game if it is won. It is possible that the user hasn't finished entering the hints, so
        //some things might have been removed when they should not have been.
        if (!guessList.getLastGuess().getHints().contains(Guess.HINT_WRONG_PLACE)
                && !guessList.getLastGuess().getHints().contains(Guess.HINT_NOT_IN_WORD)) {
            gameEnded(true);
        }
    }
}