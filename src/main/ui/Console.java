package ui;

import model.Guess;
import model.GuessList;
import model.Statistics;
import persistence.GuessListJSN;
import persistence.StatisticsJSN;
import persistence.WordList;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

// The console-based user interface
public class Console extends UI {
    private static final String[] MAIN_MENU_OPTIONS = {
            "Start a new game",
            "Return to a saved game",
            "Solve an already-started game",
            "View statistics",
            "Quit"
    };
    private static final String[] MAIN_MENU_OPTIONS_NO_SAVED = {
            "Start a new game",
            "Solve an already-started game",
            "View statistics",
            "Quit"
    };
    private static final String INVALID_OPTION = "Invalid option.";
    private static final String FIRST_GUESS_PROMPT = "Do you want to choose the first word? (y/n) ";
    private static final String HINTS_PROMPT = "Select the hint given by the game: ";
    private static final String[] HINTS_CHOICES = {
            "Green (letter is in the right spot)",
            "Yellow (letter is in the wrong spot)",
            "Gray (letter is not in the word)"
    };
    private static final String ASK_MORE_GUESSES = "Do you want to enter any more guesses? (y/n)";
    private static final String WORD_PROMPT_PREFIX = "The chosen word is: ";
    private static final String WORD_PROMPT_SUFFIX = "\nIs this word accepted? ";
    private static final String[] WORD_OPTIONS = {
            "Yes",
            "No, I don't want to use it (keep in list)",
            "No, not considered valid (delete)",
            "Use custom word",
            "Quit game"};
    private static final String QUIT_PROMPT = "Save game? ";
    private static final String[] QUIT_OPTIONS = {"Yes", "No", "Don't quit"};

    private static String nextWord;

    //MODIFIES: this
    //EFFECTS: Perform some initialization, then go to the function to ask the user what to do
    //         This is necessary because to avoid code duplication, I am using an abstract UI class to contain common
    //         code and separate Console and Graphical classes to handle the differences. However, abstract methods
    //         cannot be static, so I need to create an instance of the UI object even though it's not really useful.
    public static void main(String[] args) {
        random = new Random();
        try {
            statistics = StatisticsJSN.loadStatistics();
        } catch (FileNotFoundException e) {
            statistics = new Statistics(LOSE_AFTER);
        }
        try {
            WordList.loadWords();
        } catch (FileNotFoundException e) {
            System.out.println("Failed to read the word list!");
            System.exit(1);
        } catch (IOException e) {
            System.out.println("Failed to write the word list! Deleting invalid words will not work.");
        }

        chooseFunction();
    }

    //MODIFIES: this
    //EFFECTS: Ask the user what to do, and then call the appropriate function
    private static void chooseFunction() {
        switch (getOption(MAIN_MENU_PROMPT, MAIN_MENU_OPTIONS)) {
            case 0:
                newGame();
                break;
            case 1:
                savedGame();
                break;
            case 2:
                existingGame();
                break;
            case 3:
                viewStatistics();
                break;
            case 4:
                System.exit(0);
                break;
        }
    }

    //REQUIRES: options.length > 0
    //EFFECTS: Prompts the user to select an option out of the list given, prompting again if they do something invalid
    private static int getOption(String prompt, String[] options) {
        Scanner in = new Scanner(System.in);
        int chosenOption;
        for (int i = 0; i < options.length; i++) {
            System.out.println(i + ". " + options[i]);
        }

        while (true) {
            System.out.print(prompt);
            try {
                chosenOption = Integer.parseInt(in.nextLine());
                if (chosenOption < 0 || chosenOption >= options.length) {
                    System.out.println(INVALID_OPTION);
                } else {
                    break;
                }
            } catch (NumberFormatException e) {
                System.out.println(INVALID_OPTION);
            }
        }

        return chosenOption;
    }

    //EFFECTS: Asks the user a yes or no question, returning true if they answer yes or false if they answer no. Any
    //         input starting with a y is considered yes, and any input starting with an n is considered no. Anything
    //         else is invalid and will result in the question being asked again.
    //TODO add (y/n) in here so the same strings can be shared between GUI and console
    private static boolean promptYesNo(String prompt) {
        Scanner in = new Scanner(System.in);
        boolean choice;

        while (true) {
            System.out.print(prompt);
            String userInput = in.nextLine();
            if (userInput.charAt(0) == 'y' || userInput.charAt(0) == 'Y') {
                choice = true;
                break;
            } else if (userInput.charAt(0) == 'n' || userInput.charAt(0) == 'N') {
                choice = false;
                break;
            } else {
                System.out.println(INVALID_OPTION);
            }
        }
        return choice;
    }

    //EFFECTS: Show the user how well the algorithm has been performing using data from the statistics object
    protected static void viewStatistics() {
        for (int i = 1; i <= LOSE_AFTER; i++) {
            System.out.print(i);
            System.out.print(" attempts: ");
            System.out.print(statistics.getWinRateOfAllWins(i) * 100);
            System.out.print("% of wins, ");
            System.out.print(statistics.getWinRateOfAllGames(i) * 100);
            System.out.println("% of games.");
        }

        System.out.print("Average winning guesses is ");
        System.out.println(statistics.getAverageWinningGuess());

        System.out.print("Won ");
        System.out.print(statistics.getWinRateOfAllGames() * 100);
        System.out.print("%, lost ");
        System.out.print(statistics.getLossRateOfAllGames());
        System.out.print("% of ");
        System.out.print(statistics.getTotalGames());
        System.out.println(" games.");
    }

    //MODIFIES: this
    //EFFECTS: Lets the user enter multiple guesses at once, without being bugged by suggestions. Useful for when they
    //         already played some of the game before starting to use the solver, so they can get it to the right state
    //         quickly without being interrupted by prompts.
    protected static void existingGame() {
        guessList = new GuessList(LOSE_AFTER);
        String word = getCustomWord();
        List<Integer> hints = getHintsFromUser();
        guessList.add(new Guess(word, hints));
        while (guessList.gameInProgress() && promptYesNo(ASK_MORE_GUESSES)) {
            word = getCustomWord();
            hints = getHintsFromUser();
            guessList.add(guessList.getLastGuess().makeNextGuess(word, hints));
        }
        playGame(guessList);
    }

    //MODIFIES: this
    //EFFECTS: Starts a new game, with either a random word or a user-chosen word
    private static void newGame() {
        guessList = new GuessList(LOSE_AFTER);
        String word;
        if (promptYesNo(FIRST_GUESS_PROMPT)) {
            word = getCustomWord();
        } else {
            List<String> allWords = WordList.getWordList();
            while (true) {
                word = allWords.get(random.nextInt(allWords.size()));
                if (promptYesNo("The chosen word is " + word + ". Is that accepted? (y/n) ")) {
                    break;
                } else {
                    try {
                        WordList.deleteWord(word);
                    } catch (IOException e) {
                        System.out.println("Failed to delete word. It will continue to appear in the future.");
                    }
                }
            }
        }
        List<Integer> hints = getHintsFromUser();
        guessList.add(new Guess(word, hints));
        playGame(guessList);
    }

    //MODIFIES: this
    //EFFECTS: Let the user load a saved game from the filesystem and resume it.
    private static void savedGame() {
        try {
            guessList = GuessListJSN.loadGuessList();
            playGame(guessList);
        } catch (FileNotFoundException e) {
            switch (getOption(MAIN_MENU_PROMPT_NO_SAVED, MAIN_MENU_OPTIONS_NO_SAVED)) {
                case 0:
                    newGame();
                    break;
                case 1:
                    existingGame();
                    break;
                case 2:
                    viewStatistics();
                    break;
                case 3:
                    System.exit(0);
                    break;
            }
        }
    }

    //EFFECTS: Let the user enter a word, repeating if the word is invalid, and return the word entered by the user
    private static String getCustomWord() {
        String word;
        Scanner in = new Scanner(System.in);
        do {
            System.out.print("Enter a word into Wordle to see if it is valid, then enter it here if it is accepted: ");
            word = in.next();
        } while (word.length() != Guess.WORD_LENGTH);
        return word.toLowerCase();
    }

    //EFFECTS: Gets the user to provide the hints given to them by the game. Returns them as a hint List ready to be
    //         given to the Guess constructor.
    //TODO validate hints to make sure they don't conflict with earlier hints
    //TODO allow backtracking to fix mistakes
    private static List<Integer> getHintsFromUser() {
        List<Integer> hints = new ArrayList<Integer>(Guess.WORD_LENGTH);
        for (int i = 0; i < Guess.WORD_LENGTH; i++) {
            switch (getOption(HINTS_PROMPT, HINTS_CHOICES)) {
                case 0:
                    hints.add(Guess.HINT_CORRECT);
                    break;
                case 1:
                    hints.add(Guess.HINT_WRONG_PLACE);
                    break;
                case 2:
                    hints.add(Guess.HINT_NOT_IN_WORD);
                    break;
            }
        }
        return hints;
    }

    //A Comparator used to sort words in the order that they should be tried
    private static class WordComparator implements Comparator<String> {
        private static final char[] VOWELS = {'a', 'e', 'i', 'o', 'u'};

        //EFFECTS: Returns a decimal value representing the portion of characters that are vowels
        private double getVowelPortion(String string) {
            int vowelCount = 0;
            for (char character: string.toCharArray()) {
                if (Arrays.asList(VOWELS).contains(character)) {
                    vowelCount++;
                }
            }
            return (double)vowelCount / string.length();
        }

        @Override
        //EFFECTS: Returns -1 if o1 should go first, 1 if o2 should go first, and 0 if they are equal
        public int compare(String o1, String o2) {
            int letterCount1 = (int) o1.chars().distinct().count();
            int letterCount2 = (int) o2.chars().distinct().count();

            //Try words with a higher number of distinct letters first
            if (letterCount1 > letterCount2) {
                return -1;
            } else if (letterCount1 < letterCount2) {
                return 1;
            } else {
                //If they have the same number of unique words, try the one with the higher portion of vowels first
                double vowelPortion1 = getVowelPortion(o1);
                double vowelPortion2 = getVowelPortion(o2);
                if (vowelPortion1 > vowelPortion2) {
                    return -1;
                } else if (vowelPortion2 > vowelPortion1) {
                    return -1;
                } else {
                    //If this is also the same, the words are considered equally good.
                    return 0;
                }
            }
        }
    }

    //REQUIRES: nextWords.size() > 0
    //MODIFIES: nextWords, this
    //EFFECTS: Prompts the user what to do and either:
    //          accepts the proposed word,
    //          rejects the proposed word but does not delete it,
    //          rejects and deletes the proposed word,
    //          gets a word from the user to use instead,
    //          quits the game
    //         Returns true if another word should be pulled from the list of candidates, or false if we're done
    private static boolean askUserAboutNextWord(List<String> nextWords) {
        switch (getOption(WORD_PROMPT_PREFIX + nextWord + WORD_PROMPT_SUFFIX, WORD_OPTIONS)) {
            case 0:
                return true;
            case 1:
                nextWords.remove(nextWord);
                return false;
            case 2:
                guessList.getLastGuess().getNextWords().remove(nextWord);
                try {
                    WordList.deleteWord(nextWord);
                } catch (IOException e) {
                    System.out.println("Failed to delete the word. It will be included again next time.");
                }
                nextWords.remove(nextWord);
                return false;
            case 3:
                nextWord = getCustomWord();
                return true;
            case 4:
                quitGame(guessList);
                return false;
        }
        return false;
    }

    //REQUIRES: guessList.getNumGuesses() >= 1
    //MODIFIES: guessList
    //EFFECTS: Repeatedly suggests words from the last guess's possible word list, get hints from the user, and then
    //         generate more Guess objects until eventually the game ends.
    protected static void playGame(GuessList guessList) {
        while (guessList.gameInProgress()) {
            List<String> nextWords = new ArrayList<String>(guessList.getLastGuess().getNextWords());
            nextWords.sort(new WordComparator());
            if (nextWords.size() == 0) {
                System.out.println("All known words are eliminated. You are on your own. Good luck!");
                guessList.loseGame();
                break;
            }
            // nextWord should be declared here, but it isn't because checkstyle made me split this method into several
            // methods, so it needs to be a static class field.
            while (true) {
                try {
                    nextWord = nextWords.get(0);
                    // Doing spaghetti code here just to keep the method length short enough to stop checkstyle from
                    // complaining
                    if (askUserAboutNextWord(nextWords)) {
                        break;
                    }
                } catch (IndexOutOfBoundsException e) {
                    System.out.println("There are no more words. Please try again.");
                    nextWords = new ArrayList<String>(guessList.getLastGuess().getNextWords());
                    nextWords.sort(new WordComparator());
                }
            }
            guessList.add(guessList.getLastGuess().makeNextGuess(nextWord, getHintsFromUser()));
        }
        gameCompleted(guessList);
    }

    //EFFECTS: Attempts to save the state if the user wants to, and asks the user again if they really want to quit
    private static void quitGame(GuessList guessList) {
        switch (getOption(QUIT_PROMPT, QUIT_OPTIONS)) {
            case 0:
                try {
                    GuessListJSN.saveGuessList(guessList);
                } catch (IOException e) {
                    if (promptYesNo(SAVE_FAILED_PROMPT)) {
                        System.exit(0);
                    } else {
                        return;
                    }
                }
                System.exit(0);
            case 1:
                System.exit(0);
                break;
            case 2:
                return;
        }
    }

    private static void gameCompleted(GuessList guessList) {
        if (guessList.gameWon()) {
            statistics.addWin(guessList.getNumGuesses());
            System.out.println("The game is won!");
        } else {
            statistics.addLoss();
            System.out.println("The game is lost.");
        }
        try {
            StatisticsJSN.writeStatistics(statistics);
        } catch (IOException e) {
            System.out.println("Failed to update the statistics");
        }
    }
}
