package ui;

import model.Event;
import model.EventLog;
import model.Guess;
import model.GuessList;
import model.Statistics;
import persistence.GuessListJSN;
import persistence.StatisticsJSN;
import persistence.WordList;

import javax.swing.*;
import java.awt.event.WindowListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class Graphical extends UI {
    private static final String[] MAIN_MENU_OPTIONS = {
            "Start a new game",
            "Return to a saved game",
            "Solve an already-started game",
            "Quit"
    };
    private static final String[] MAIN_MENU_OPTIONS_NO_SAVED = {
            "Start a new game",
            "Solve an already-started game",
            "Quit"
    };
    private static final String FIRST_GUESS_PROMPT = "Do you want to choose the first word?";
    private static final String ASK_MORE_GUESSES = "Do you want to enter any more guesses?";

    public static final String CONTROL_BUTTON_LABEL_NEXT_GUESS = "Next guess";
    public static final String CONTROL_BUTTON_LABEL_CUSTOM_GUESS = "Custom guess";
    public static final String CONTROL_BUTTON_LABEL_DELETE_GUESS = "Delete last guess";
    public static final String CONTROL_BUTTON_LABEL_QUIT = "Quit";
    private static final String[] CONTROL_BUTTON_LABELS = {
            CONTROL_BUTTON_LABEL_NEXT_GUESS,
            CONTROL_BUTTON_LABEL_CUSTOM_GUESS,
            CONTROL_BUTTON_LABEL_DELETE_GUESS,
            CONTROL_BUTTON_LABEL_QUIT
    };

    private static List<Integer> initialHints;
    private static HintAreaUpdater hintAreaUpdater;
    protected static ControlButtonHandler controlButtonHandler;
    protected static List<List<JButton>> guessPanelButtons;
    private static WindowListener windowListener;

    //MODIFIES: this
    //EFFECTS: Perform some initialization, then go to the function to ask the user what to do
    //         This is necessary because to avoid code duplication, I am using an abstract UI class to contain common
    //         code and separate Console and Graphical classes to handle the differences. However, abstract methods
    //         cannot be static, so I need to create an instance of the UI object even though it's not really useful.
    public static void main(String[] args) {
        random = new Random();
        initialHints = new ArrayList<>();
        hintAreaUpdater = new HintAreaUpdater();
        controlButtonHandler = new ControlButtonHandler();
        windowListener = new WordleWindowListener();

        for (int i = 0; i < Guess.WORD_LENGTH; i++) {
            initialHints.add(Guess.HINT_NOT_IN_WORD);
        }
        try {
            statistics = StatisticsJSN.loadStatistics();
        } catch (FileNotFoundException e) {
            statistics = new Statistics(LOSE_AFTER);
        }
        try {
            WordList.loadWords();
        } catch (FileNotFoundException e) {
            alert("Failed to read the word list!");
            printLogAndExit(1);
        } catch (IOException e) {
            alert("Failed to write the word list! Deleting invalid words will not work.");
        }

        chooseFunctionAndDoSetup();
        openMainWindow();
    }

    //EFFECTS: Generate the panel that contains the area to display guesses and set hints
    private static JPanel generateGuessPanel() {
        JPanel guessPanel = new JPanel();
        guessPanel.setLayout(new BoxLayout(guessPanel, BoxLayout.Y_AXIS));
        guessPanelButtons = new ArrayList<>();
        for (int i = 0; i < LOSE_AFTER; i++) {
            JPanel row = new JPanel();
            row.setLayout(new BoxLayout(row, BoxLayout.X_AXIS));
            guessPanelButtons.add(new ArrayList<>());
            for (int j = 0; j < Guess.WORD_LENGTH; j++) {
                JButton button = new JButton(" ");
                button.setActionCommand(String.valueOf(i) + ":" + String.valueOf(j));
                button.addActionListener(hintAreaUpdater);
                row.add(button);
                guessPanelButtons.get(i).add(button);
            }
            guessPanel.add(row);
        }
        return guessPanel;
    }

    //EFFECTS: Generate the panel that contains the display of statistics
    private static JPanel generateStatsPanel() {
        JPanel statsPanel = new JPanel();
        statsPanel.setLayout(new BoxLayout(statsPanel, BoxLayout.Y_AXIS));
        statsPanel.add(new JLabel("Wins in _ rounds:"));
        for (int i = 1; i <= LOSE_AFTER; i++) {
            JPanel row = new JPanel();
            row.setLayout(new BoxLayout(row, BoxLayout.X_AXIS));
            row.add(new JLabel(String.valueOf(i) + ": "));
            JProgressBar bar = new JProgressBar(0, 100);
            bar.setValue((int) (statistics.getWinRateOfAllGames(i) * 100));
            row.add(bar);
            statsPanel.add(row);
        }
        JPanel row = new JPanel();
        row.setLayout(new BoxLayout(row, BoxLayout.X_AXIS));
        row.add(new JLabel("All: "));
        JProgressBar bar = new JProgressBar(0, 100);
        bar.setValue((int) (statistics.getWinRateOfAllGames() * 100));
        row.add(bar);
        statsPanel.add(row);
        return statsPanel;
    }

    //EFFECTS: Generates a panel that contains the buttons on the bottom
    private static JPanel generateControlButtonPanel() {
        JPanel controlButtonArea = new JPanel();
        for (String label: CONTROL_BUTTON_LABELS) {
            JButton button = new JButton(label);
            button.setActionCommand(label);
            button.addActionListener(controlButtonHandler);
            controlButtonArea.add(button);
        }
        return controlButtonArea;
    }

    //MODIFIES: this
    //EFFECTS: Creates the application's main window
    private static void openMainWindow() {
        JFrame frame = new JFrame("Wordle Solver");
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));

        JPanel mainArea = new JPanel();
        mainArea.setLayout(new BoxLayout(mainArea, BoxLayout.X_AXIS));
        mainArea.add(new JLabel(new ImageIcon("data/logo.png")));
        mainArea.add(generateGuessPanel());
        mainArea.add(generateStatsPanel());
        frame.add(mainArea);

        frame.add(generateControlButtonPanel());

        frame.pack();
        frame.setVisible(true);
        frame.addWindowListener(windowListener);
        hintAreaUpdater.updateButtons();
    }

    //REQUIRES: options.length > 0, options does not contain any blank strings
    //EFFECTS: Prompts the user to select an option out of the list given, prompting again if they do something invalid
    private static int getOption(String prompt, String[] options) {
        String chosenOption = (String) JOptionPane.showInputDialog(null, prompt, prompt,
                JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

        if (chosenOption == null) {
            return getOption("You are not allowed to press cancel, "
                            + "but I can't figure out how to get rid of the button.",
                    options);
        } else {
            return Arrays.asList(options).indexOf(chosenOption);
        }
    }

    //MODIFIES: this
    //EFFECTS: Ask the user what to do, and then call the appropriate function to do the setup.
    private static void chooseFunctionAndDoSetup() {
        switch (getOption(MAIN_MENU_PROMPT, MAIN_MENU_OPTIONS)) {
            case 0:
                newGame();
                break;
            case 1:
                savedGame();
                break;
            case 2:
                existingGame();
                break;
            case 3:
                printLogAndExit(0);
                break;
        }
    }

    //EFFECTS: Asks the user a yes or no question, returning true if they answer yes or false if they answer no.
    protected static boolean promptYesNo(String prompt) {
        int choice = JOptionPane.showConfirmDialog(null, prompt, prompt, JOptionPane.YES_NO_OPTION);
        return choice == JOptionPane.YES_OPTION;
    }

    //EFFECTS: Let the user enter a word, repeating if the word is invalid, and return the word entered by the user
    private static String getCustomWord() {
        String word;
        do {
            word = (String) JOptionPane.showInputDialog(
                    null,
                    "Enter a word into Wordle to see if it is valid, then enter it here if it is accepted.",
                    "Custom word",
                    JOptionPane.QUESTION_MESSAGE,
                    null,
                    null,
                    ""
            );
        } while (word == null || word.length() != Guess.WORD_LENGTH);
        return word.toLowerCase();
    }

    //EFFECTS: Show an alert and return when the user closes it.
    protected static void alert(String message) {
        JOptionPane.showConfirmDialog(null, message, message, JOptionPane.PLAIN_MESSAGE);
    }

    //MODIFIES: wordList
    //EFFECTS: Choose a good word from the word list, returning the first accepted by the user, and removing words that
    //         are rejected
    private static String chooseWord(List<String> wordList) {
        String word;
        while (true) {
            word = wordList.get(random.nextInt(wordList.size()));
            if (promptYesNo("The chosen word is " + word + ". Is that accepted?")) {
                break;
            } else {
                try {
                    wordList.remove(word);
                    WordList.deleteWord(word);
                } catch (IOException e) {
                    alert("Failed to delete word. It will continue to appear in the future.");
                }
            }
        }
        return word;
    }

    //MODIFIES: this
    //EFFECTS: Starts a new game, with either a random word or a user-chosen word
    private static void newGame() {
        String word;
        guessList = new GuessList(LOSE_AFTER);
        if (promptYesNo(FIRST_GUESS_PROMPT)) {
            word = getCustomWord();
        } else {
            word = chooseWord(WordList.getWordList());
        }
        guessList.add(new Guess(word, initialHints));
    }

    //MODIFIES: this
    //EFFECTS: Let the user load a saved game from the filesystem and resume it.
    private static void savedGame() {
        try {
            guessList = GuessListJSN.loadGuessList();
        } catch (FileNotFoundException e) {
            switch (getOption(MAIN_MENU_PROMPT_NO_SAVED, MAIN_MENU_OPTIONS_NO_SAVED)) {
                case 0:
                    newGame();
                    break;
                case 1:
                    existingGame();
                    break;
                case 2:
                    printLogAndExit(0);
                    break;
            }
        }
    }

    //MODIFIES: this
    //EFFECTS: Lets the user enter multiple guesses at once, without being bugged by suggestions. Useful for when they
    //         already played some of the game before starting to use the solver, so they can get it to the right state
    //         quickly without being interrupted by prompts.
    protected static void existingGame() {
        guessList = new GuessList(LOSE_AFTER);
        String word = getCustomWord();
        guessList.add(new Guess(word, initialHints));
        while (guessList.gameInProgress() && promptYesNo(ASK_MORE_GUESSES)) {
            word = getCustomWord();
            guessList.add(guessList.getLastGuess().makeNextGuess(word, initialHints));
        }
        if (guessList.getNumGuesses() == LOSE_AFTER) {
            gameEnded(false);
        }
    }

    //REQUIRES: guessList.getNumGuesses() >= 1
    //MODIFIES: this
    //EFFECTS: End the game if it should be ended. If customWord, let the user choose a word. Otherwise, choose a word.
    //         Add it to the guess list.
    protected static void playRound(boolean customWord) {
        if (guessList.getLastGuess().getNextWords().size() > 0) {
            if (customWord) {
                guessList.add(guessList.getLastGuess().makeNextGuess(getCustomWord(), initialHints));
            } else {
                Guess lastGuess = guessList.getLastGuess();
                guessList.add(lastGuess.makeNextGuess(chooseWord(lastGuess.getNextWords()), initialHints));
            }
            hintAreaUpdater.updateButtons();
            //Don't check if we lost yet: hints aren't set yet.
        } else {
            gameEnded(false);
        }
    }

    //EFFECTS: Displays an appropriate message based on whether the game was won or lost, and exit.
    protected static void gameEnded(boolean won) {
        if (won) {
            statistics.addWin(guessList.getNumGuesses());
            alert("The game is won!");
        } else {
            statistics.addLoss();
            alert("The game is lost.");
        }
        try {
            StatisticsJSN.writeStatistics(statistics);
        } catch (IOException e) {
            alert("Failed to update the statistics");
        }
        printLogAndExit(0);
    }

    //EFFECTS: Print the log to the console, and then exit with the given status
    protected static void printLogAndExit(int status) {
        for (Event event: EventLog.getInstance()) {
            System.out.println(event.toString());
        }
        System.exit(status);
    }
}
