package ui;

import persistence.GuessListJSN;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import static ui.Graphical.*;
import static ui.UI.SAVE_FAILED_PROMPT;
import static ui.UI.guessList;

//Handles the control buttons at the bottom of the main window
public class ControlButtonHandler implements ActionListener {
    //MODIFIES: Graphical
    //EFFECTS: If there is more than one guess, delete the last guess
    //         If not, alert the user
    private void deleteGuess() {
        if (guessList.getInternalList().size() > 1) {
            guessList.getInternalList().remove(guessList.getInternalList().size() - 1);
        } else {
            alert("Cannot delete the first guess!");
        }
    }

    //EFFECTS: Ask the user if they want to save, and give them one last chance to avoid quitting.
    protected void quit() {
        int saveGame = JOptionPane.showConfirmDialog(null, "Do you want to save?",
                "Save?", JOptionPane.YES_NO_CANCEL_OPTION);
        if (saveGame == JOptionPane.YES_OPTION) {
            try {
                GuessListJSN.saveGuessList(guessList);
                printLogAndExit(0);
            } catch (IOException ex) {
                if (promptYesNo(SAVE_FAILED_PROMPT)) {
                    printLogAndExit(0);
                }
            }
        } else if (saveGame == JOptionPane.NO_OPTION) {
            printLogAndExit(0);
        } //If the popup was cancelled, do nothing
    }

    //REQUIRES: e is a string present in CONTROL_BUTTON_LABELS
    //MODIFIES: this
    //EFFECTS: Performs the action appropriate for the control button that was pressed.
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();
        switch (actionCommand) {
            case CONTROL_BUTTON_LABEL_NEXT_GUESS:
                playRound(false);
                break;
            case CONTROL_BUTTON_LABEL_CUSTOM_GUESS:
                playRound(true);
                break;
            case CONTROL_BUTTON_LABEL_DELETE_GUESS:
                deleteGuess();
                break;
            default:
                quit();
                break;
        }
    }
}